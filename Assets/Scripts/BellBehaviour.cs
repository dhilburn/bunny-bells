﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BellBehaviour : MonoBehaviour 
{
    [HideInInspector]
    public bool addJump;

    void Start()
    {
        int addChance = Random.Range(0, 100);

        if (addChance == 99)
            addJump = true;
        else
            addJump = false;

        if (addJump)
            GetComponent<SpriteRenderer>().color = new Vector4(0, 1, 1, 1);
    }
}
