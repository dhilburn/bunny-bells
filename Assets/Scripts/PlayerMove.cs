﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

enum GameMode
{
    KEYBOARD,
    MOUSE,
    TOUCH
}

public class PlayerMove : MonoBehaviour
{
    public float speed;
    public float jumpForce = 10f;
    public float mulligans;
    [SerializeField] GameMode gameMode;

    float currentY;
    float maxReachedY;

    int takenMuls;
    int collectedBells;
    int tapCount;

    Rigidbody2D bunBody;
    float maxX = 2.83f;

    Text currentHeightText,
         bestHeightText,
         mulligansText,
         bellsText;

    Vector3 moveDirection;

    // Use this for initialization
    void Start ()
    {
        bunBody = GetComponent<Rigidbody2D>();

        InitializeTrackedVars();
    }
	
	// Update is called once per frame
	void Update ()
    {
        moveDirection = Vector3.zero;

        UpdateHeight();
        UpdateDisplays();

        MulliganJump();

        if (gameMode == GameMode.KEYBOARD)
            KeyboardControl(ref moveDirection);
        else if (gameMode == GameMode.MOUSE)
            MouseControl(ref moveDirection);
        else if (gameMode == GameMode.TOUCH)
            TouchControl(ref moveDirection);

        if (moveDirection != Vector3.zero)
        {
            transform.Translate(moveDirection * Time.deltaTime * speed);
            ConstrainBunnyToScreen();            
            FlipBunnySprite(moveDirection);
        }        
	}    

    void InitializeTrackedVars()
    {
        maxReachedY = currentY = transform.position.y;
        takenMuls = 0;
        collectedBells = 0;
        InitializeTrackedVarDisplays();
    }

    void InitializeTrackedVarDisplays()
    {
        currentHeightText = GameObject.Find("Current Height").GetComponent<Text>();
        bestHeightText = GameObject.Find("Best Height").GetComponent<Text>();
        mulligansText = GameObject.Find("Jumps").GetComponent<Text>();
        bellsText = GameObject.Find("Collected Bells").GetComponent<Text>();
    }

    void UpdateHeight()
    {
        currentY = transform.position.y;
        if (currentY > maxReachedY)
            maxReachedY = currentY;
    }

    void UpdateDisplays()
    {
        currentHeightText.text = currentY.ToString("F3");
        bestHeightText.text = maxReachedY.ToString("F3");
        mulligansText.text = (mulligans - takenMuls).ToString();
        bellsText.text = collectedBells.ToString();
    }

    void KeyboardControl(ref Vector3 movementDirection)
    {
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            movementDirection += Vector3.left;
        }
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            movementDirection += Vector3.right;
        }
    }

    void MouseControl(ref Vector3 movementDirection)
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //mousePos.y = 0f;
        //mousePos.z = 0f;

        movementDirection = mousePos - transform.position;
        movementDirection.y = 0;
        movementDirection.z = 0;
    }

    void TouchControl(ref Vector3 movementDirection)
    {
        if (Input.touchCount > 0)
        {
            Touch firstTouch = Input.touches[0];
            Vector3 touchPos = Camera.main.ScreenToWorldPoint(firstTouch.position);

            if (firstTouch.phase == TouchPhase.Moved)
                tapCount = 0;

            movementDirection = touchPos - transform.position;
            movementDirection.y = 0;
            movementDirection.z = 0;
        }
    }

    void ConstrainBunnyToScreen()
    {
        if (Mathf.Abs(transform.position.x) >= maxX)
        {
            if (transform.position.x < 0)
                transform.position = new Vector3(-maxX, transform.position.y);
            else
                transform.position = new Vector3(maxX, transform.position.y);
        }
    }

    void FlipBunnySprite(Vector3 movementDirection)
    {
        if (movementDirection.x > 0)
            transform.localScale = new Vector3(-1, transform.localScale.y);
        else if (movementDirection.x < 0)
            transform.localScale = new Vector3(1, transform.localScale.y);
    }

    void MulliganJump()
    {
        if (gameMode == GameMode.KEYBOARD || gameMode == GameMode.MOUSE)
        {
            if (Input.GetMouseButtonDown(0) && takenMuls < mulligans)
            {
                bunBody.velocity = new Vector3(bunBody.velocity.x, jumpForce);
                takenMuls++;
            }
        }
        else if (gameMode == GameMode.TOUCH)
        {
            if (Input.touchCount > 1 && takenMuls < mulligans)
            {
                if (tapCount < 2)
                    tapCount++;
                else
                {
                    bunBody.velocity = new Vector3(bunBody.velocity.x, jumpForce);
                    takenMuls++;
                    tapCount = 0;
                }
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Bell"))
        {
            bunBody.velocity = new Vector3(bunBody.velocity.x, jumpForce);
            Destroy(other.gameObject);
            collectedBells++;
            if (collectedBells % 100 == 0)
                takenMuls--;
            if (other.GetComponent<BellBehaviour>().addJump)
                takenMuls--;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        takenMuls = 0;
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        takenMuls = 0;
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
