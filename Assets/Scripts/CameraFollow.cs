﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;
    public float distance = 10f;

    float startZ;

	// Use this for initialization
	void Start ()
    {
        startZ = transform.position.z;
	}
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 correctedPos = Vector3.zero;

        correctedPos.y = target.position.y - distance;
        correctedPos.z = startZ;

        transform.position = correctedPos;
	}
}
