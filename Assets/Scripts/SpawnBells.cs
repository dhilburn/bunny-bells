﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBells : MonoBehaviour
{
    [Tooltip("Bell object to spawn.")]
    public GameObject spawnObj;
    [Tooltip("How high above the player bells will spawn in at.")]
    public float distanceAbovePlayer;
    [Tooltip("Reference to the player's transform.")]
    public Transform player;
    [Tooltip("How far to the sides bells can spawn.")]
    public float sideLimit;
    [Tooltip("How many bells can be spawned in at one time.")]
    public float spawnLimit;


    GameObject lastSpawn;


	// Use this for initialization
	void Start ()
    {
        Vector3 spawnPos = new Vector3(Random.Range(-sideLimit, sideLimit), player.transform.position.y + distanceAbovePlayer, 0);
        lastSpawn = Instantiate(spawnObj, spawnPos, Quaternion.identity);
        StartCoroutine("Spawn");        
        //InvokeRepeating("BellSpawner", 0.5f, 0.67f);
	}

    void BellSpawner()
    {
        float xPos = Random.Range(-sideLimit, sideLimit);
        Vector3 spawnPos = new Vector3(xPos, player.transform.position.y, 0);
        spawnPos.y += distanceAbovePlayer;

        lastSpawn = Instantiate(spawnObj, spawnPos, Quaternion.identity);
    }

    IEnumerator Spawn()
    {
        while(true)
        {
            float xPos = Random.Range(-sideLimit, sideLimit);
            Vector3 spawnPos = new Vector3(xPos, player.transform.position.y, 0);
            spawnPos.y += distanceAbovePlayer;

            lastSpawn = Instantiate(spawnObj, spawnPos, Quaternion.identity);

            yield return new WaitForSeconds(Random.Range(0.33f, 0.75f));
        }
    }
}
